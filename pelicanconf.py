#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = u'Fábio Luciano'
SITENAME = u'Não importa'
SITESUBNAME= u'Devaneios de um DesDevOps'
SITEURL = ''

TIMEZONE = 'America/Sao_Paulo'

DEFAULT_LANG = u'br'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None


DEFAULT_CATEGORY = 'sem catetogia'


# Blogroll
LINKS =  (('Pelican', 'http://getpelican.com/'),
          ('Python.org', 'http://python.org/'),
          ('Jinja2', 'http://jinja.pocoo.org/'),
          ('You can modify those links in your config file', '#'),)

# Social widget
SOCIAL = (('You can add links in your config file', '#'),
          ('Another social link', '#'),)

FEED_ALL_ATOM = 'feeds/all.atom.xml'
FEED_ALL_RSS = 'feeds/all.rss.xml'

STATIC_PATHS = [
    'extras/robots.txt',
    'extras/humans.txt',
    'images',
    'extras/CNAME'
]
EXTRA_PATH_METADATA = {
    'extras/robots.txt': {'path': 'robots.txt'},
    'extras/humans.txt': {'path': 'humans.txt'},
    'extras/CNAME': {'path': 'CNAME'},
    }

ARTICLE_URL = 'post/{slug}/'
ARTICLE_SAVE_AS = 'post/{slug}/index.html'

PAGE_URL ='page/{slug}/'
PAGE_SAVE_AS = 'page/{slug}/index.html'


PLUGIN_PATH = '../github_clones/others/pelican-plugins'
PLUGINS = ['liquid_tags.img', 'liquid_tags.video',
           'liquid_tags.youtube', 'liquid_tags.include_code', 'multi_part', 'neighbors', 'optimize_images', 'related_posts',  'sitemap']

DEFAULT_PAGINATION = 5

THEME = '../github_clones/me/nerdtheme'

ADDTHIS_PROFILE = 'ra-4ed1b4e8215fb8b7'
DISQUS_SITENAME = 'noimporta'
USE_OPEN_GRAPH = True
OPEN_GRAPH_FB_APP_ID = '690666366'
OPEN_GRAPH_IMAGE = 'images/mimimi.png'
GITHUB_USER = 'fabioluciano'
GITHUB_SKIP_FORK = True
GOOGLE_ANALYTICS = 'UA-24614996-1'
GOOGLE_ADSENSE_CA = 'ca-pub-6728499030452827'
GOOGLE_ADSENSE_SLOT = '7493278399'
PYGMENTS_RST_OPTIONS = {'classprefix': 'pgcss', 'linenos': 'table'}

SITEMAP = {
    'format': 'xml',
    'priorities': {
        'articles': 0.5,
        'indexes': 0.5,
        'pages': 0.5
    },
    'changefreqs': {
        'articles': 'monthly',
        'indexes': 'daily',
        'pages': 'monthly'
    }
}


DELETE_OUTPUT_DIRECTORY = True

# Uncomment following line if you want document-relative URLs when developing
RELATIVE_URLS = True
