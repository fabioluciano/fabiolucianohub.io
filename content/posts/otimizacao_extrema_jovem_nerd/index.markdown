Title: Jovem nerd
Date: 2010-12-03 10:20
Category: Python
Tags: pelican, publishing
Author: Alexis Metaireau
<!-- Status: Draft -->

#Estudo de caso - Jovem Nerd

![Logotipo JovemNerd](http://i.imgur.com/INwtgLh.png)

Há alguns anos, minhas sextas-feiras têm mais sentido quando se trata de entretenimento quando me privo de sair para conversar e beber com os amigos, além disso, o trajeto de casa para o trabalho e vice-e-versa se tornou mais divertido. Tudo isso graças ao grupo mais NERD que existe hoje na internet: **O Jovem Nerd**.
Se você, como eu, tem interesse na área da tecnologia, é bem provável que já tenha ouvido falar deles. Começaram pequenos, tentando conquistar um nicho que antes era esquecido(os NERDs), e que hoje é *mainstream*. Seu principal produto é o NerdCast, um programa que semanalmente trata de um assunto, não necessáriamente do mundo nerd. Se você ainda não sabe quem são, vai uma dica: Corra! Baixe o maior número de episódios que você conseguir e dê mais sentido a sua nerdice, e saiba que você não está sozinho.

## O problema
No dia 07 de fevereiro de 2012, o grupo estreou um novo layout[^1], substituindo um layout que acompanhava o site há vários anos. O problema é que durante os primeiros acessos, havia vários problemas perfomáticos, parte devido ao grande número de pessoas que estavam passando para dar uma olhada na nova cara do site. Na época, ao acessar, era bem provável você receber um erro 500, ou mesmo a página demorar mais de um minuto para ser carregada.

Alguns meses depois, acessando o site religiosamente ao menos uma vez por dia, visto que o nerdcast é apenas um dos produtos do grupo Jovem Nerd, comecei a perceber que os problemas que havia identificado quando o novo layout foi lançado permaneciam. O principal problema identificado foi a demora excessiva do término de carregamento da página.
Então, para ilustrar que é possível diminuir drasticamente o tempo de carregamento e mais importante, diminuir a quantidade de bytes que trafegam do servidor ao navegador do usuário, e o número de requisições feitas ao servidor.

## Disclaimer ou "Panos quentes"
Antes de continuar com este artigo, acredito ser importante esclarecer alguns pontos sobre como foram conseguidos os dados que estão disponíveis ao longo do documento.
**Nenhum**, veja bem **NENHUM** dado foi capturado de maneira ilícita. Todos eles foram capturados com ferramentas espécíficas para esse tipo de tarefa, e todos são públicos e disponíveis se você estiver procurando o que procuro com esse artigo.
Dito isso, continuemos com os trabalhos...

## Alguns números como justificativa
Para justificar este artigo, estou utilizando o firebug, que na aba rede, mostra todas as trocas de mensagens do navegador com o servidor do objeto de estudo em questão. Os dados gerados pela extensão foram gravados em um arquivos har[^2][^3][^4], e utilizei a gem har[^3] para tranformar os dados em informação. Passados 6 minutos com a extensão aberta, chegamos a seguinte situação:

### Página inicial do portal

![Estatísticas da página inicial](http://i.imgur.com/cFN8H9P.png)

#### Números por tamanho

Para exibir a página foram executadas **273** requisições.
O tamanho da página, juntando todos seus elementos acumularam **6.2MB**, sendo:

> * **206** imagens, totalizando **5.1MB** *
> * **6** páginas, totalizando **28.1KB** *
> * **36** arquivos JavaScript, totalizando **886.4KB** *
> * **17** arquivos CSS, totalizando **49.3KB** *
> * **1** arquivo Flash, totalizando **8.3KB** *
> * **7** outros arquivos, totalizando **92.6KB** *

\* Está incluído elementos provenientes de anúncios

#### Números por tempo de carregamento

Do início do carregamento da página(quando o usuário fez a requisição para o site), até o final, o tempo acumulado entre a primeira e última requisição foi de **18.23s**. Sendo:

> * **7.49s** efetuando resolução de DNS *
> * **19.68s** efetuando conexão com os servidores *
> * **5.56m** bloqueando o carregamento completo da página * **
> * **51.45s** aguardando o servidor responder a solicitação *
> * **41.1s** recebendo a resposta do servidor

\* Está incluído elementos provenientes de anúncios<br />
\** Há um arquivo reset.css que até o fim do teste, não havia terminado seu carregamento.


#### Números por envios e recebimentos
Do início do carregamento da página até seu fim, foram enviados **180.9KB** de informação e recebidas **6.2MB**. Sendo
:

> * **180.9KB** de cabeçalhos enviados *
> * **94.2KB** de cabeçalhos recebidos *
> * **6.2MB** de informação recebidas *

\* Está incluído elementos provenientes de anúncios

### Página de um Nerdcast
![Estatísticas de um episódio do nerdcast](http://i.imgur.com/hYgxEbr.png)

#### Números por tamanho
Para exibir a página foram executadas **1477** requisições.
O tamanho da página, juntando todos seus elementos acumularam **18.1MB**, sendo:

> * **1219** imagens, totalizando **14MB** *
> * **85** páginas, totalizando **541.5KB** *
> * **126** arquivos JavaScript, totalizando **3MB** *
> * **25** arquivos CSS, totalizando **57KB** *
> * **1** arquivo Flash, totalizando **8.3KB** *
> * **21** outros arquivos, totalizando **517.6KB** *

 \* Está incluído elementos provenientes de anúncios

#### Números por tempo de carregamento

Do início do carregamento da página(quando o usuário fez a requisição para o site), até o final, o tempo acumulado entre a primeira e última requisição foi de **2.79m**. Sendo:

> * **8.1s** efetuando resolução de DNS *
> * **32.1s** efetuando conexão com os servidores *
> * **9.91m** bloqueando o carregamento completo da página * **
> * **5.77m** aguardando o servidor responder a solicitação *
> * **4.45m** recebendo a resposta do servidor

\* Está incluído elementos provenientes de anúncios<br />
\** Há um arquivo reset.css que até o fim do teste, não havia terminado seu carregamento.

#### Números por envios e recebimentos
Do início do carregamento da página até seu fim, foram enviados **831.5KB** de informação e recebidas **18.5MB**. Sendo:

> * **831.5KB** de cabeçalhos enviados *
> * **427.7KB** de cabeçalhos recebidos *
> * **18.1MB** de informação recebidas *

\* Está incluído elementos provenientes de anúncios

### Página de uma notícia
![Estatísticas de uma página de notícia](http://i.imgur.com/niYby5w.png)

#### Números por tamanho
Para exibir a página foram executadas **396** requisições.
O tamanho da página, juntando todos seus elementos acumularam **5.1MB**, sendo:

> * **188** imagens, totalizando **1.9MB** *
> * **38** páginas, totalizando **239.7KB** *
> * **124** arquivos JavaScript, totalizando **2.4MB** *
> * **26** arquivos CSS, totalizando **111.6KB** *
> * **2** arquivo Flash, totalizando **305.1KB** *
> * **18** outros arquivos, totalizando **211.5KB** *

\* Está incluído elementos provenientes de anúncios

#### Números por tempo de carregamento

Do início do carregamento da página(quando o usuário fez a requisição para o site), até o final, o tempo acumulado entre a primeira e última requisição foi de **32.94s**. Sendo:

> * **4.45s** efetuando resolução de DNS *
> * **25.67s** efetuando conexão com os servidores *
> * **2.86m** bloqueando o carregamento completo da página * **
> * **1.34m** aguardando o servidor responder a solicitação *
> * **13.9s** recebendo a resposta do servidor

\* Está incluído elementos provenientes de anúncios

#### Números por envios e recebimentos
Do início do carregamento da página até seu fim, foram enviados **180.9KB** de informação e recebidas **5.1MB**. Sendo:

> * **310.9KB** de cabeçalhos enviados *
> * **145.8KB** de cabeçalhos recebidos *
> * **5.1MB** de informação recebidas *

\* Está incluído elementos provenientes de anúncios

## Problemas identificados
De certa forma, avançamos um pouco sobre tecnologia no Brasil, e a forma como a informação é apresentada aos usuários, hoje, é vista de forma diferente. Quem diria que teríamos as velocidades que dispomos com um preço "acessível". Então, com isso, surge cada vez mais sites que são especializados no entretenimento. Hoje tudo na internet se resume a imagem. Você é bombardeado diáriamente com vídeos, imagens, músicas, e são pouquíssimos os websites que se preocupam em como o entretenimento será entregue. A maior preocupação é "quem foi o primeiro a disponibilizá-lo". Felizmente, esse não é o caso do grupo Jovem Nerd, porém, com os números das análises feitas identifiquei uma série de problemas que podem afetar a experiência do usuário ao acessar o site. Vejamos eles.

### Problemas na página inicial
A análise foi feita utilizando três parâmetros: O HTML da página, os componentes que são requisitados para renderização da página, e a informação disponível do backend.

#### Análise do código fonte (HTML)
##### Tags de estilo anexadas de diversas formas ao longo do documento
Além das 17 requisições feitas ao servidor por arquivos css, vários elementos possuem estilos definidos em linha. A quantidade de arquivos de estilo dificulta a manutenção, tornando trabalhosa a tarefa de depuração. O mesmo vale às modificações feitas em linha. Além disso, aumenta a quantidade de requisições feitas ao servidor, impactando na velocidade que a página será renderizada.

##### Tags de javascript anexadas de diversas formas ao longo do documento
Além das 36 requisições feitas ao servidor por arquivos javascript, várias instruções estão codificadas ao longo do documento. A quantidade de arquivos dificulta a manutenção, tornando trabalhosa a tarefa de depuração. O mesmo vale às modificações feitas em linha. Além disso, aumenta a quantidade de requições feitas ao servidor, impactando na velocidade que a página será renderizada.

##### Tags de javascript sendo carregadas no cabeçalho do documento
12 arquivos javascript estão sendo carregados no cabeçalho do documento, sendo as tags script bloqueantes, isso quer dizer que o carregamento da página, e sua renderização só continuará, quando todos os arquivos que estão no cabeçalho forem carregados.

##### Elemento iframe
O elemento está presente em todas as páginas analisadas, fazendo com que requisitos como acessibilidade e usabilidade sejam descartados. Além disso, dentro do iframe, outro arquivo da biblioteca jQuery é carregado, dessa vez uma versão não compactada.

##### O documento possui diversos espaços
O documento acumula diversos espaços em branco, o que aumenta considerávelmente seu tamanho, fazendo com que o tempo de carregamento da página aumente.

##### Ausência de elementos semânticos
Vários elementos estão representados de forma incorreta, impossibilitando os veículos de busca interpretarem as microdatas que claramente o portal possui.

##### Retirar a codificação das tags meta

#### Análise dos componentes (CSS, JS e imagens)
##### Javascript
###### Existencia de 'fixes' para requisições multiprotocolo
###### O script que carrega o iframe para login recarrega toda a página
###### Scripts de outras páginas sendo carregados em todas as páginas
###### O script de carrossel efetua uma requisição em todas as mudanças
###### Todos os arquivos javascript são carregados no próprio servidor
##### CSS
###### Elementos de estilo em linha
Os elementos além de estarem no header devem estar usando links...
###### Todos os arquivos de estilo não estão compactados

##### Imagens
###### Todas as imagens são servidas sem nenhum tipo de otimização
###### Várias imagens são redimensionadas pelo CSS ou HTML
As imagens devem ser servidas sem nenhum tipo de redimensionamento feito pelo navegador.
###### Várias imagens não possui suas dimensões explicitadas
Todas as imagens precisam de suas dimensões explicitadas, para evitar o redesenho...reflow
######

#### Análise do backend
##### Não é especificado uma codificação previamente
##### Otimização do controle de cache de todos os arquivos
##### Não existe o paralelamento de requisições
##### Conteúdo estático servido em um domínio com cookies
##### Existência de conteúdo sem compressão GZIP

#### Conclusão

### Problemas na página de um Nerdcast
####Análise do código fonte(HTML)
#### Análise dos componentes (CSS, JS e imagens)

```js
var Busca = function(jsonEpisodio) {
    this.url = 'http://jovemnerd.com.br/nerdcast/';
    this.json = jsonEpisodio;
};

Busca.prototype = function() {
    var getLink = function(ep) {
        var that = this,
            numeroEpisodio = parseInt(ep, 10) || null,
            link = '';

        if (numeroEpisodio !== null) {
            that.json.forEach(function(episodio) {
                if (episodio.numero === numeroEpisodio) {
                    link = that.url + episodio.permalink;
                }
            });
        }

        return link;
    },
        getAllByParticipante = function(part, cond) {
            var that = this,
                participantes = part || '',
                condicional = cond || false,
                episodios = [];

            if (typeof participantes === 'object' && Array.isArray(participantes)) {
                if (condicional === true) {
                    episodios = that.json.filter(function(episodio) {
                        var contador = 0;

                        episodio.participantes.forEach(function(participante) {
                            if (participantes.indexOf(participante) !== -1) {
                                contador++;
                            }
                        });

                        if (contador === participantes.length) return true;
                    });
                } else {
                    participantes.forEach(function(participante) {
                        episodios = episodios.concat(that.byParticipante(participante));
                    });
                }
            } else {
                that.json.forEach(function(episodio) {
                    if (episodio.participantes.indexOf(participantes) !== -1) {
                        episodios.push(episodio);
                    }
                });
            }

            return episodios;
        },
        getAllByTema = function(tem, cond) {
            var that = this,
                temas = tem || '',
                condicional = cond || false,
                episodios = [];

            if (typeof temas === 'object' && Array.isArray(temas)) {
                if (condicional === true) {
                    episodios = that.json.filter(function(episodio) {
                        var contador = 0;

                        episodio.temas.forEach(function(tema) {
                            if (temas.indexOf(tema) !== -1) {
                                contador++;
                            }
                        });

                        if (contador === temas.length) return true;
                    });
                } else {
                    temas.forEach(function(tema) {
                        episodios = episodios.concat(that.byTema(tema));
                    });
                }
            } else {
                that.json.forEach(function(episodio) {
                    if (episodio.temas.indexOf(temas) !== -1) {
                        episodios.push(episodio);
                    }
                });
            }

            return episodios;
        };

    return {
        getLink: getLink,
        byParticipante: getAllByParticipante,
        byTema: getAllByTema
    };
}();
```

```js
var episodios = [{
    numero: 391,
    titulo: 'Ela dança, eu danço. Ou não',
    permalink: 'nerdcast-391-ela-danca-eu-danco-ou-nao',
    participantes: ['Affonso Solano', 'Atila', 'Edney Souza', 'Guga', 'Nick Ellis', 'Tucano'],
    temas: ['']
}, {
    numero: 341,
    titulo: 'O Corvo, a Periguete e o Bucentauro',
    permalink: 'nerdcast-341-especial-rpg-o-corvo-a-periguete-e-o-bucentauro',
    participantes: ['Carlos Voltor', 'JP', 'Rex', 'Tucano'],
    temas: ['RPG']
}, {
    titulo: "Isso é coisa de f#dido! 2",
    permalink: "nerdcast-301-isso-e-coisa-de-fdido-2",
    numero: "301",
    participantes: ['Allotoni', 'Azaghal'],
    temas: ['Comportamento', 'Cotidiano']
}];
```

```js
var b = new Busca(episodios);

console.log('Objetos com o numero 391');
console.log(b.getLink(391));

console.log('Objetos com o numero "391"');
console.log(b.getLink("391"));

console.log('Objetos com o numero 666');
console.log(b.getLink(666));

console.log('-----');

console.log('Objetos com o participante: Rex');
console.log(b.byParticipante('Rex'));

console.log('Objetos com o participante: [Rex]');
console.log(b.byParticipante(['Rex']));

console.log('Objetos com o participante: Fábio');
console.log(b.byParticipante('Fábio'));

console.log('Objetos com os participantes: Rex OU Tucano');
console.log(b.byParticipante(['Rex', 'Tucano']));

console.log('Objetos com os participantes: Rex OU Fábio');
console.log(b.byParticipante(['Rex', 'Fábio']));

console.log('Objetos com os participantes: Rex E Tucano');
console.log(b.byParticipante(['Rex', 'Tucano'], true));

console.log('Objetos com os participantes: Rex E Fábio');
console.log(b.byParticipante(['Rex', 'Fábio'], true));

console.log('-----');

console.log('Objetos com o tema: Comportamento');
console.log(b.byTema('Comportamento'));

console.log('Objetos com o tema: [Comportamento]');
console.log(b.byTema(['Comportamento']));

console.log('Objetos com o tema: Religião');
console.log(b.byTema('Religião'));

console.log('Objetos com os temas: Compartamento OU RPG');
console.log(b.byTema(['Comportamento', 'RPG']));

console.log('Objetos com os temas: Comportamento OU Religião');
console.log(b.byTema(['Comportamento', 'Religião']));

console.log('Objetos com os temas: Compartamento E RPG');
console.log(b.byTema(['Comportamento', 'Cotidiano'], true));

console.log('Objetos com os temas: Comportamento E Religião');
console.log(b.byTema(['Comportamento', 'Religião'], true));
```

#### Análise do backend
#### Conclusão

### Problemas na página de uma notícia
####Análise do código fonte(HTML)
#### Análise dos componentes (CSS e JS)

#### Análise do backend
#### Conclusão

## Plano de ataque
Soluções aos problemas identificados



[^1]: [Lambda lambda lambda! BEM-VINDOS AO NOVO JOVEM NERD!!!!!](http://jovemnerd.com.br/comunicados/lambda-lambda-lambda-bem-vindos-ao-novo-jovem-nerd/)

[^2]: [HARs de todas páginas avaliadas](https://www.dropbox.com/s/yxjd7bj114rmwo7/arquivos-har.tar.gz)

[^3]: [Gem Har](http://rubygems.org/gems/har)

[^4]: [Melhoria na busca de episódios](http://jsbin.com/aBobIXU/7/edit?js,console)

[^5]: [Resultados obtidos de websitetest](http://www.websitetest.com/ui/tests/52cb38718b5f023252000079/samples/52cb38dbf613f241900006ac)

