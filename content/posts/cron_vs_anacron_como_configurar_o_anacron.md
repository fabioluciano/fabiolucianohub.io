Title: Cron Vs Anacron: Como configurar o Anacron no Linux
Date: 2013-01-11 00:00
Tags: anacron, cron
Category: Linux

O Anacron é o cron para desktops e notebooks. O Anacron não espera que o sistema continue rodando 24 x 7, como um servidor.
Quando você precisar que um job fique automaticamente rodando em backgroud em uma máquina que não roda como um servidor, você deve usar o Anacron.

Por exemplo, se você tem um script de backup agendado para todos os dias às 23hrs como um job comum do cron, e se seu notebook não estiver ligado às 23hrs, seu script de backup não será executado.

No entanto, se você tiver o mesmo job agendado no anacron, você pode ter certeza que ele será executado assim que o notebook for ligado.

## Formato do Anacrontab

Assim como o cron tem o arquivo /etc/crontab, o anacron tem o /etc/anacrontab

O arquivo /etc/anacrontab tem seus jobs anacron mencionados no seguinte formato:


    período     atraso     Identificador do Job     comando

**O primeiro campo é o período de recorrência: **Esse será um valor numérico que especificará o número de dias até o job ser executado.

* 1 – diariamente (daily)
* 7 – semanalmente (weekly)
* 30 – mensalmente (monthly)
* N – Esse pode ser qualquer valor numérico. N indica o número de dias

Nota: Você também pode usar ‘@monthly’ para um job que precisa ser executado mensalmente.

**O segundo campo é o atraso: **Esse indica o tempo de atraso em minutos. Isto é, a quantidade de minutos que o anacron deve esperar, após a máquina ser ligada, para executar o job.

**O terceiro campo é o identificador do job:** É o nome para o arquivo que guardará o timestamp do job. Ele deve ser único para cada job. Ele estará disponível em um arquivo dentro do diretório /var/spool/anacron. Este arquivo conterá uma única linha indicando a última vez que o job foi executado.


    ls -l /var/spool/anacron/
    test.daily
    cron.daily
    cron.monthly
    cron.weekly

**O quarto campo é o comando:** O comando ou script shell que precisa ser executado.

Assim como em scripts shell, comentários dentro do arquivo anacrontrab começam com #

## Exemplo Anacron

O exemplo a seguir execute o script /home/fabioluciano/backup.sh a cada 7 dias.

No dia em que o script o job com o backup.sh supostamente deveria ser executado, se o sistema estiver desligado por qualquer razão, o anacron vai executar o job, 15 minutos depois que a máquina for ligada(não tendo que esperar mais sete dias)


    cat /etc/anacrontab
    7       15      test.daily      /bin/sh /home/fabioluciano/backup.sh

## START_HOURS_RANGE e RANDOM_DELAY

O exemplo acima indica que o script backup.sh deve ser executado todos os dias, com um atraso de 15 minutos, isto é, quando o notebook for ligado, execute apenas após 15 minutos passados.

O que acontece quando o notebook ou desktop não estiver desligado? Quando esses jobs serão executados? Isso é especificado pela variável de ambiente **START_HOURS_RANGE** no arquivo /etc/anacrontab.

Por padrão é setado no arquivo para 3-22. Isso indica o intervalo de tempo de 03:00hrs até 22:00hrs.

    grep START /etc/anacrontab
    START_HOURS_RANGE=3-22

Acima do valor de atraso especificado no segundo campo no arquivo /etc/anacrontab, o anacron também adiciona randomicamente x número de minutos ao atraso. O x é definido pela variável **RANDOM_DELAY** no arquivo /etc/anacrontab.

Por padrão é setado no arquivo para 45. Isso significa que o anacron vai adicionar x minutos(randomicamente escolhidos entre 0 e 45), e adicionar ao atraso definido pelo usuário.

    grep RANDOM /etc/anacrontab
    RANDOM_DELAY=45

## Cron Vs Anacron

<table class="table table-striped">
  <thead>
    <tr>
      <th>Cron</th>
      <th>Anacron</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Menor granularidade é em minutos (isto é, jobs podem ser agendados para serem executados à cada minuto)</td>
      <td>Menor granularidade é somente em dias</td>
    </tr>
    <tr>
      <td>Cron Job pode ser agendando por usuários normais (se não for restrito pelo super usuário)</td>
      <td>de fazê-lo usável por usuários normais)</td>
    </tr>
    <tr>
      <td>Cron Job pode ser agendando por usuários normais (se não for restrito pelo super usuário)</td>
      <td>Anacron pode ser usado somente pelo super usuário (mas existem meios de fazê-lo usável por usuários normais)</td>
    </tr>
    <tr>
      <td>Cron espera por sistemas rodando 24 x 7. Se um job foi agendando, e o sistema estiver desligado durante o horário setado, o job não será executado.</td>
      <td>Anacron não espera que o sistema esteja rodando 24 x 7. Se um job foi agendando, e o sistema estiver desligado durante o horário setado, ele executará os jobs assim que o sistema estiver ligado</td>
    </tr>
    <tr>
      <td>Ideal para servidores</td>
      <td>Ideal para desktops e notebooks</td>
    </tr>
    <tr>
      <td>Use o cron quando um job precisa ser executado em uma hora e minuto em particular</td>
      <td>Use o anacron quando um job precisa ser executado independente da hora e minuto</td>
    </tr>
  </tbody>
</table>
