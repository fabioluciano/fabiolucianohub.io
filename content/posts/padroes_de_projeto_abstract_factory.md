Title: Padrões de projeto - Abstract Factory
Date: 2013-01-11 00:00
Tags:  padrões de projeto
Category: Qualidade de Software

Começando com a nossa jornada a compreensão dos padrões de projeto que usaremos no futuro de nossos projetos para torná-los mais fáceis de manter, começaremos com o design pattern **Abstract Factory**

## Tipo de padrão

Criacional

## Intenções

*   Prover uma interface para criar famílias de objetos relacionados ou dependentes, sem especificar suas classes concretas.
*   Criar uma hierarquia que encapsula: algumas possíveis &#8220;Plataformas&#8221;, e a construção de um grupo de &#8220;produtos&#8221;.
*   O operador _new_ é considerado prejudicial a estrutura.

## Aplicabilidade

Devemos usar o padrão Abstract Factory quando:

*   O sistema precisa de indenpendência de como os produtos se comportam quando são criados.
*   O sistema precisa ou precisará ser configurado para trabalhar com diversas famílias de produtos.
*   Uma família de produtos é desenhada para trabalhar apenas quando todos juntos.
*   Uma biblioteca de produtos é necessária, para as quais é relevante não somente a interface, mas a implementação também.

## Diagrama

![Diagrama da Estrutura de Classes](http://yuml.me/diagram/scruffy/class/%5BCliente%5D-%3E%5BConvert_Abstraction_AbstractFactory%5D,%20%5BConvert_Abstraction_AbstractFactory%5D%20%20%3C-%20%5BAbstract_Factory_Convert_JPG%5D,%20%5BConvert_Abstraction_AbstractFactory%5D%20%20%3C-%20%5BAbstract_Factory_Convert_PNG%5D,%20%5BAbstract_Factory_Convert_JPG%5D%20%3C-%20%5BConvert_Abstraction_Factory%5D,%20%5BAbstract_Factory_Convert_PNG%5D%20%3C-%20%5BConvert_Abstraction_Factory%5D)

## Exemplo

Nosso exemplo é composto por quatro classes principais, sendo elas:

*   **Convert_Abstraction_AbstractFactory** - A Fábrica em sí. Direciona o Fluxo de acordo com a especialização.
*   **Convert_Abstraction_Factory** - O Nosso Objeto Abstrato, responsável pelas definições comuns aos objetos produtos.
*   **Abstract_Factory_Convert_PNG** e **Abstract_Factory_Convert_JPG** - Nossos objetos Concretos.

## Objeto abstrato

A classe abaixo representa o nosso objeto abstrato, que como você verá não será invocado nenhuma vez por nenhum script, apenas estanciado pelos objetos concretos. É importante notar também a presença das constantes que serão usadas pelos objetos concretos e a interface, contendo o método que deve ser comum a todos os objetos concretos.

    <?php

    interface iFactory {
        public function image_convert($img);
    }

    abstract class Convert_Abstraction_Factory  implements iFactory {

        const IMG_PATH  = './img';
        const TMP_PATH  = './tmp';
        const CONV_PATH = './conv';

        protected function __construct() {}

        protected function to_pnm($img) {
            $time = time();

            exec('anytopnm ' . self::IMG_PATH . DIRECTORY_SEPARATOR . $img . ' > ' . self::TMP_PATH . DIRECTORY_SEPARATOR . $time .  '.pnm');

            return $time;
        }

    }

## Objetos concretos

Nas classes abaixo estão representados os objetos concretos de nossa classe, os produtos que de fato serão servidos ao cliente que está no topo de nosso diagrama. Fiz apenas duas classes de especialização, uma para converter para PNG e outra para converter para JPG, mas você pode adicionar quantas especializações achar necessário.

    <?php
    class Abstract_Factory_Convert_JPG extends Convert_Abstraction_Factory {

        const EXTENSION = '.jpg';

        public function __construct() {
            parent::__construct();
        }

        public function image_convert($img) {
            $name = $this->to_pnm($img);
            exec('pnmtojpeg ' . self::TMP_PATH . DIRECTORY_SEPARATOR . $name .  '.pnm > ' . self::CONV_PATH . DIRECTORY_SEPARATOR . $name . self::EXTENSION);
        }

    }

PNG

    <?php
    class Abstract_Factory_Convert_PNG extends Convert_Abstraction_Factory {

        const EXTENSION = '.png';

        public function __construct() {
            parent::__construct();
        }

        public function image_convert($img) {
            $name = $this->to_pnm($img);
            exec('pnmtopng ' . self::TMP_PATH . DIRECTORY_SEPARATOR . $name .  '.pnm > ' . self::CONV_PATH . DIRECTORY_SEPARATOR . $name . self::EXTENSION);
        }

    }

## A Fábrica

Aqui fica o grande truque de nosso script, onde tudo faz sentido. A partir do valor inserido no método contrutor, o script vai decidir qual objeto concreto ele irá usar.

    <?php

    class Convert_Abstraction_AbstractFactory {

        public function __construct() {

        }

        public static function convert($to) {

            require_once('Convert_Abstraction_Factory.php');

            switch ($to) {
                case 'png':
                    require_once 'Convert_Abstraction_Factory_PNG.php';
                    $factory = new Abstract_Factory_Convert_PNG();
                    break;
                case 'jpg':
                    require_once 'Convert_Abstraction_Factory_JPG.php';
                    $factory = new Abstract_Factory_Convert_JPG();
                    break;
            }

            return $factory;
        }

     }

Instanciando e usando a fábrica criada

    <?php

    $abstractfactory = new Convert_Abstraction_AbstractFactory();

    $factory = $abstractfactory::convert('png');
    $factory->image_convert('jpg.jpg')
