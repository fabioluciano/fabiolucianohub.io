Title: Padrões de projeto - Introdução
Date: 2013-01-11 00:00
Tags:  padrões de projeto
Category: Qualidade de Software

Já se perguntou o que são padrões de projeto? Nesta série, vou explicar por que padrões de projeto são importantes, e quando e por que aplicá-los.

## O que são Padrões de Projeto?

Padrões de projeto, mais conhecidos como Design Patterns, são a solução reusável, e otimizados para problemas em nossos programas que encontramos todos os dias. Um padrão de projeto é simplesmente uma classe ou uma biblioteca que você pluga em seu sistema; é muito mais que isso. É um modelo que deve ser implementado usando o bom senso, nas situações corretas. Não é específico de cada linguagem de programação. Um bom padrão de projeto pode ser implementado na maioria - se não em todas - as linguagens de programação, dependendo apenas das capacidades da linguagem escolhida. Mais importante ainda, qualquer padrão de projeto pode ser uma faca de dois gumes, que se implementado de forma incorreta ou no lugar errado, pode ser desastroso e trazer diversos problemas para você. Entretanto, se implementado no lugar certo, no momento certo, pode mudar o rumo do seu projeto, tornando-o muito mais fácil de manejá-lo.

Existem três tipos básico de padrões de projeto. São eles:

## Por que devemos usá-los?

Padrões de projeto são, por princípio, soluções bem pensadas para problemas de programação. Muitos programadores encontraram esses problemas anteriormente e usaram essas soluções para remediá-las. Se você encontrou algum desses problemas, por que recriar uma solução se alguém já passou por esses problemas e tem a melhor solução para você?

Ao longo desta série irie falar sobre todos os padrões de projeto, fazendo da mesma forma que foi descrito pela GoF(Gang of Four), apresentado de forma estruturada, dizendo seu nome, seu tipo, definição, estrutura e sua aplicação em código.

Acredito que ao final da série, você terá bagagem o suficiente para criar projetos de qualidade usando padrões de projeto.

## Padrões de Projeto Criacionais

Padrões de projeto criacionais são padrões que lidam com o mecanismo de criação de objetos, tentando criar objetos das formas mais adequadas possíveis. A forma básica de criação de objetos pode resultar em problemas ou adicionar complexidades no projeto. Padrões de projeto criacionais, resolvem esses problemas controlando, de alguma forma, a criação desses objetos. Padrões de projeto criacionais ainda são categorizados em padrões criacionais de objetos e padrões criacionais de classes, onde os padrões criacionais de objetos lidam com a criação de objetos, e os de criacionais de classe lidam com a instancialização das classes. De forma resumida, este padrão lida com problemas que podem aparecer na criação e instancialização de objetos.

Alguns exemplos de padrões criacionais são:

  * **[Abstract factory][1]**
  * **Factory method**
  * **Builder pattern**
  * **Lazy initialization**
  * **Object pool**
  * **Prototype**
  * **Singleton**

## Padrões de Projeto Estruturais

Padrões de projeto estruturais são padrões que lidam com as estruturas do projeto, facilitando a comunicação entre suas entidades. Por enquanto, esse conceito parmancerá abstrato, mas de acordo com os padrões deste tipo, entenderemos melhor. Em resumo, estes padrões, em outras palavras cuidam da estrutura de seu projeto. Por outro lado padrões estruturais devem ser aplicados em classes responsáveis pela estrutura dos domínios, fazendo uma analogia com a engenharia civil, eles seriam responsáveis por definir o alicerce da construção bem como a estrutura para sustentá-la.

Alguns exemplos de padrões estruturais são:

  * **Adapter**
  * **Bridge**
  * **Composite**
  * **Decorator**
  * **Facade**
  * **Flyweight**
  * **Private Class Data**
  * **Proxy**

## Padrões de Projeto Comportamentais

Padrões de projeto comportamentais referem-se a identificação de padrões comuns de comunicação entre objetos, tornando-os mais fácil e flexíveis de manipulá-los

  * **Chain of responsibility**
  * **Command**
  * **Interpreter**
  * **Iterator**
  * **Mediator**
  * **Memento**
  * **Null Object**
  * **Observer**
  * **State**
  * **Strategy**
  * **Template method**

   [1]: http://naoimporta.com/serie-padroes-de-projeto-abstract-factory

