Title: Wake up Loser: Medida para acordar programador malemolente
Date: 2013-01-11 00:00
Tags:  pynotify, python, xprintidle
Category: Programação


    :::python
    #! /usr/bin/python
    # -*- coding: iso-8859-1 -*-
    # Dependencias pynotify e xprintidle

    import commands
    import time
    import pynotify

    msg = 'Computador idle por mais de 5 minutos. Pegue café.'
    limit = 3000000

    while 1:
      idle = int(commands.getoutput('xprintidle'))
      if idle > limit:
          notification = pynotify.Notification("Cafeinne", msg)
          notification.show()
      time.sleep(60)
