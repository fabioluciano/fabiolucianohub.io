Title: Minhas cinco razões para não usar nomes de variáveis curtos
Date: 2013-01-11 00:00
Tags:  boas práticas, legibilidade, programação, tradução, variáveis
Category: Programação

Outro dia desses esbarrei numa linha como essa revisando código:

    sysMgrH = vFgrMan

ou

    parse1(l, e, arr);
    parse2(l2, e2, arr);


Por favor, não faça mais isso! Escolha nomes de variáveis longos e claros, nada mais! Por que?

  * Atualmente é permitido ter nomes de variáveis maiores que um, e ainda maiores que dez caracteres, se você precisar.
  * Seu trabalho não é economizar tempo digitando menos ou trinta segundos, trata-se de produzir algo de alta qualidade. E algo que dê pra mater. E sustentável.
  * Na verdade, escrever o código é apena 1% do seu tempo, comparando com o resto, como por exemplo, pensar o que se deve escrever. Você pode também perder alguns segundos na parte da escrita.
  * Se você se preocupa em ter de escrever o mesmo nome de variável de 12 caracteres várias vezes: faça amizade com Ctrl %2B Space.
  * Mas minha razão atual, minha principal razão é a seguinte: você não escreve essa parte do código para você e para agora. Isso continuará existindo pelos próximos cinco ou dez anos, e se você tiver sorte, dezenas de desenvolvedores irão ver isso. E alguns deles, precisarão entendẽ-lo. Em dois meses, em um ano, em cinco. Mas para eles, não é uma questão de 30 segundos (que é o tempo que você economizou cinco anos atrás, lembra?), tenha em mente que, em média, mais de 50% dos custos de desenvolvimento de software são gastos após a versão inicial está concluída.

Então aqui vai o meu apelo: seja legal e escreva o código como se você escrever um romance. Escolha palavras agradáveis, escolha palavras longas, e formate tudo bem. Pense no seu colega desenvolvedor, no futuro, que vai agradecer por isso.

Post Original: http://www.hendrikbeck.com/2010/08/03/my-5-reasons-not-to-use-short-variable-names-anymore/
